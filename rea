#!/bin/bash

# Exit on any error
set -e

###
# Default parameters
#
DEFAULT_TTY=/dev/ttyUSB0
# Always set these in case the .rea script doesn't set one
board='salvator-x'
vrcar='2.23.0'
drv='full'
win='wayland'
desc='rea'
wind=${win/wayland/weston}

###
# ANSI colors and print functions
#
export NORMAL='\e[0m'
export BRED='\e[31;1m'
export BGREEN='\e[32;1m'
export BYELLOW='\e[33;1m'
export BBLUE='\e[34;1m'
export BMAGENTA='\e[35;1m'
export BCYAN='\e[36;1m'
export BWHITE='\e[37;1m'


export poky_v270='c8e5c38b8a36cbb45831fcd8469bd96068ae300c'
export linaro_v270='acf4f1f701e07670ec88435897a74f88dbe8ba87'
export openembedded_v270='dc5634968b270dde250690609f0015f881db81f2'
export renesas_v270='318c9b7daa44219a2f44e0966c17ae58266452c0'

export poky_v280='6dba9abd43f7584178de52b623c603a5d4fcec5c'
export linaro_v280='acf4f1f701e07670ec88435897a74f88dbe8ba87'
export openembedded_v280='c305ac5d2f5285d5eec8952a4ca7f3b4f89aed96'
export renesas_v280='1d6ca1f7d9080c4807f60ed477be5311b107b16d'

export poky_v290='65306b0bfc1afd0de9b1d470fd78c8c69f55f791'
export linaro_v290='acf4f1f701e07670ec88435897a74f88dbe8ba87'
export openembedded_v290='cb7e68f2a39fa6f24add48fc7b8d38fb7291bb44'
export renesas_v290='20319d147591f9aaf2f32bf2d74abe188b538d61'

export poky_v2120='40376446904ae3529be41737fed9a0b650ed167d'
export linaro_v2120='9b1fd178309544dff1f7453e796a9437125bc0d9'
export openembedded_v2120='8ab04afbffb4bc5184cfe0655049de6f44269990'
export renesas_v2120='522efd09043aa8af1eec01af2b9e025df424d9a5'

export poky_v2160='yocto-2.1.2'
export linaro_v2160='2f51d38048599d9878f149d6d15539fb97603f8'
export openembedded_v2160='55c8a76da5dc099a7bc3838495c672140cedb78e'
export renesas_v2160='3613b2780a6b5d5d70ea6802be5060a8214cbdb5'

export poky_v2190='yocto-2.1.2'
export linaro_v2190='2f51d38048599d9878f149d6d15539fb97603f8f'
export openembedded_v2190='55c8a76da5dc099a7bc3838495c672140cedb78e'
export renesas_v2190='95cb48ba09bc7e55fd549817e3e26723409e68d5'

export poky_v2230='yocto-2.1.2'
export linaro_v2230='2f51d38048599d9878f149d6d15539fb97603f8f'
export openembedded_v2230='55c8a76da5dc099a7bc3838495c672140cedb78e'
export renesas_v2230='52a5df36b4cd2ca0f5ec8eab9446f4f128ef727c'

function pretty_size() {
	if [ -e "$1" ]; then
		size=`stat --printf="%s" $1`
		if [ "$size" -gt "1073741824" ]; then
			size=`echo $(($size/1073741824))`
			printf "$size GB"
		elif [ "$size" -gt "1048576" ]; then
			size=`echo $(($size/1048576))`
			printf "$size MB"
		elif [ "$size" -gt "4095" ]; then
			size=`echo $(($size/1024))`
			printf "$size kB"
		else
			printf "$size B"
		fi
		return $size
	else
		return -1
	fi
}

###
# Build configuration
#
function config() {
	local boards
	if [ -d "meta-renesas/meta-rcar-gen3/docs/sample/conf" ]; then
		boards=`ls -1 meta-renesas/meta-rcar-gen3/docs/sample/conf`
	elif [ -d "meta-renesas/meta-rcar-gen2/conf/machine" ]; then
		boards=`ls -1 meta-renesas/meta-rcar-gen2/conf/machine/|grep "\.conf"|awk -F. '{ print $1 }'`
	fi


	if [ -e .rea ]; then
		source .rea
		wind=${win/wayland/weston}
	else
		printf "${BYELLOW}.rea configuration file does not exist; writing default configuration${NORMAL}\n\n"
		printf "export board='%s'\n" $board >> .rea
		printf "export vrcar='%s'\n" $vrcar>> .rea
		printf "export drv='%s'\n" $drv >> .rea
		printf "export win='%s'\n" $win >> .rea
		printf "export desc='%s'\n" $desc >> .rea
	fi

	if [ -d "meta-renesas/meta-rcar-gen3/docs/sample/conf" ] || [ -d "meta-renesas/meta-rcar-gen2/conf/machine" ]; then
		if [[ ${boards} =~ (^|[[:space:]])"$board"($|[[:space:]]) ]]; then
			printf "\n"
		else
			printf "\n"
			printf "${BRED}Unsupported board! ${NORMAL}(${BBLUE}%s${NORMAL})\n" "$board"
			printf "${BGREEN}Supported boards:\n${BBLUE}"
			printf "%s\n" ${boards}
			printf "${NORMAL}\n"
		fi
	else
		printf "${BYELLOW}meta-renesas not checked out; cannot enumerate supported boards.${NORMAL}\n"
	fi

	export version=v`echo ${vrcar} | tr -d '.'`
	export BUILD_DIR=./build-${board}-${drv}-${win}-${desc}
	export BUILDNAME="renesas-rcar-${version}-${board}-${win}-${drv}-${desc}"

	printf "${BGREEN}Configuration: \n"
	printf "${BGREEN}\tBoard      : ${BBLUE}%s\n" "${board}"
	printf "${BGREEN}\tR-Car      : ${BBLUE}v%s\n" "${vrcar}"
	printf "${BGREEN}\tDrivers    : ${BBLUE}%s\n" "${drv}"
	printf "${BGREEN}\tCompositor : ${BBLUE}%s\n" "${win}"
	printf "${BGREEN}\tDescription: ${BBLUE}%s\n" "${desc}"
	printf "${BGREEN}\tBuild Name : ${BBLUE}%s\n" "${BUILDNAME}"
	printf "${BGREEN}\tBuild Dir  : ${BBLUE}%s\n" "${BUILD_DIR}"
	printf "${NORMAL}\n"
}

config



###
# sub-functions
#

function setup2() {
	local POKY=$1
	local METALINARO=$2
	local METAOE=$3
	local METARENESAS=$4

	#sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib build-essential chrpath socat libsdl1.2-dev
	# Clone the Yocto Poky repository and checkout supported revision
	if [ ! -d "poky" ]; then
		printf "${BWHITE}Cloning Yocto/poky ...${NORMAL}\n"
		git clone git://git.yoctoproject.org/poky
	fi
	printf "\n"

	if [ -d "poky" ]; then
		printf "${BWHITE}Ensuring poky is at tag %s ...${NORMAL}\n" "${POKY}"
		git -C ./poky checkout ${POKY}
	else
		printf "${BRED}poky repository not found!${NORMAL}\n"
		false
	fi
	printf "\n"


	# Get specific version of meta-linaro
	if [ ! -d "meta-linaro" ]; then
		printf "${BWHITE}Cloning meta-linaro ...${NORMAL}\n"
		git clone git://git.linaro.org/openembedded/meta-linaro.git
	fi

	if [ -d "meta-linaro" ]; then
		printf "${BWHITE}Ensuring meta-linaro is at %s ...${NORMAL}\n" "${METALINARO}"
		git -C meta-linaro checkout ${METALINARO}
	else
		printf "${BRED}meta-linaro repository not found!${NORMAL}\n"
		false
	fi
	printf "\n"

	# Get specific version of meta-openembedded
	if [ ! -d "meta-openembedded" ]; then
		printf "${BWHITE}Cloning meta-openembedded ...${NORMAL}\n"
		git clone git://git.openembedded.org/meta-openembedded
	fi

	if [ -d "meta-linaro" ]; then
		printf "${BWHITE}Ensuring meta-openembedded is at %s ...${NORMAL}\n" "${METAOE}"
		git -C meta-openembedded checkout ${METAOE}
	else
		printf "${BRED}meta-linaro repository not found!${NORMAL}\n"
		false
	fi
	printf "\n"

	# Get specific version of meta-renesas
	if [ ! -d "meta-renesas" ]; then
		printf "${BWHITE}Cloning meta-renesas ...${NORMAL}\n"
		git clone git://github.com/renesas-rcar/meta-renesas.git
	fi

	if [ -d "meta-renesas" ]; then
		printf "${BWHITE}Ensuring meta-renesas is at %s ...${NORMAL}\n" "${METARENESAS}"
		git -C meta-renesas checkout ${METARENESAS}
		printf "\n"
		cd meta-renesas
		local PATCH_DIR=$PWD/meta-rcar-gen3/docs/sample/patch/patch-for-linaro-gcc
		if [ ! -f "meta-rcar-gen3/README.linaro" ]; then
			printf "Patching GCC ...\n"
			patch -p1 < ${PATCH_DIR}/0001-rcar-gen3-add-readme-for-building-with-Linaro-Gcc.patch
		fi
		cd -
	else
		printf "${BRED}meta-renesas repository not found!${NORMAL}\n"
		false
	fi
	printf "\n"

	# Get latest meta-renesas-rea
	if [ ! -d "meta-renesas-rea" ]; then
		printf "${BWHITE}Cloning meta-renesas-rea ...${NORMAL}\n"
		git clone ssh://rea@rea.kilnhg.com/cockpit_public/yocto/meta-renesas-rea || \
			git clone https://rea.kilnhg.com/Code/cockpit_public/yocto/meta-renesas-rea.git
	fi

	# Unpack R-Car drivers
	export GFX=renesas_rcar_drivers_${version}.txz
	if [[ "${drv}" == "" || "${drv}" == "gfx-only" || "${drv}" == "full" ]]; then
		if [ -e ${GFX} ]; then
			printf "${BWHITE}Extracting %s ...${NORMAL}\n" "${GFX}"
			tar xf ${GFX}
		else
			printf "${BRED}%s not found!${NORMAL}\n" "${GFX}"
			false
		fi
	fi

	# Copy
	printf "${BWHITE}Copying proprietary drivers into renesas recipes ...${NORMAL}\n"
	if [ "${drv}" == "eval" ]; then
		cd meta-renesas
		./meta-rcar-gen3/docs/sample/copyscript/copy_proprietary_softwares.sh ../drivers_eval/
		cd -
	else
		cd meta-renesas
		./meta-rcar-gen3/docs/sample/copyscript/copy_proprietary_softwares.sh ../drivers/
		cd -
	fi
	printf "\n"
	printf "\n"

	#Start Poky environment
	printf "${BWHITE}Sourcing Poky environment ...${NORMAL}\n"
	# This will change our directory to ${BUILD_DIR}
	source ./poky/oe-init-build-env ${BUILD_DIR}

	#
	printf "${BWHITE}Configuring poky to build for Renesas %s devkit.${NORAML}\n" "${board}"

	if [[ "${drv}" == "" || "${drv}" == "full" ]]; then
		printf "Graphics and multimedia codec accelerator support running %s.\n" "${win}"
	fi

	if [[ "${drv}" == "bsp" ]]; then
		printf "BSP-only (no graphics and no multimedia codec accelerators, command-line only)\n"
	fi

	if [[ "${drv}" == "gfx-only" ]]; then
		printf "GFX-only (no multimedia codec accelerators) running %s.\n" "${win}"
	fi

	if [[ "${drv}" == "eval" ]]; then
		printf "Evaluation drivers - ${BRED}Graphics driver and multimedia codec accelerators cease operation after 1 hour.${NORMAL}\n"
	fi

	local conf=${bsp}
	if [[ "$conf" == "" || "$conf" == "full" ]]; then
		conf='mmp'
	fi

	if [[ "$win" == "wayland" ]]; then
		cp ../meta-renesas/meta-rcar-gen3/docs/sample/conf/${board}/linaro-gcc/${conf}/bblayers.conf ./conf/
		cp ../meta-renesas/meta-rcar-gen3/docs/sample/conf/${board}/linaro-gcc/${conf}/local-wayland.conf ./conf/local.conf
	else
		cp ../meta-renesas/meta-rcar-gen3/docs/sample/conf/${board}/linaro-gcc/${conf}/bblayers.conf ./conf
		cp ../meta-renesas/meta-rcar-gen3/docs/sample/conf/${board}/linaro-gcc/${conf}/local.conf ./conf/
	fi
	printf "\n"

	fix_local_conf
	fix_bblayer_conf

	printf "${BWHITE}Preparing environment to build with bitbake ...${NORMAL}\n"
	# Ensure bitback executes
	bitbake --help 1>/dev/null 2>/dev/null && printf "${BGREEN}Bitbake is ready!${NORMAL}\n" || printf "${BRED}Bitbake returned error code: %s${NORMAL}\n" "$?"
	printf "\n"
	printf "Check your bblayer.conf file (under 'build-xxxx') before building.\n"
	printf "\n"
}

function fix_local_conf() {
	# Build SDK for this machine
	printf "${BWHITE}Configuring SDK target ${mach} ...${NORMAL}\n"
	export mach=`uname -m`
	sed -i 's|^#SDKMACHINE|SDKMACHINE|' ./conf/local.conf
	sed -i "s|^SDKMACHINE ?= \".*\"|SDKMACHINE ?= \"${mach}\"|" ./conf/local.conf
	printf "SDKMACHINE set to %s\n" "${mach}"

	printf "${BWHITE}Configuring download directory ../downloads ...${NORMAL}\n"
	sed -i 's|^#DL_DIR|DL_DIR|' ./conf/local.conf
	sed -i "s|^DL_DIR ?= \".*\"|DL_DIR ?= \"\${TOPDIR}/../../downloads\"|" ./conf/local.conf
	printf "DL_DIR set to %s\n" "\${TOPDIR}/../../downloads"

	printf "${BWHITE}Configuring sstate cache directory ../sstate-cache ...${NORMAL}\n"
	sed -i 's|^#SSTATE_DIR|SSTATE_DIR|' ./conf/local.conf
	sed -i "s|^SSTATE_DIR ?= \".*\"|SSTATE_DIR ?= \"\${TOPDIR}/../sstate-cache\"|" ./conf/local.conf
	printf "SSTATE_DIR set to %s\n" "\${TOPDIR}/../sstate-cache"

	printf "${BWHITE}Configuring package format ...${NORMAL}\n"
	sed -i 's|^#PACKAGE_CLASSES|PACKAGE_CLASSES|' ./conf/local.conf
	sed -i "s|^PACKAGE_CLASSES ?= \".*\"|PACKAGE_CLASSES ?= \"package_ipk\"|" ./conf/local.conf
	printf "PACKAGE_CLASSES set to %s\n" "\"package_ipk\""
}

function fix_bblayer_conf() {
	printf "${BWHITE}Adding custom layers to bblayers.conf ...${NORMAL}\n"
	if [ "${desc}" == "rea" ]; then
		sed -i '/meta-renesas\//a \ \ \${TOPDIR}\/..\/meta-renesas-rea \\' ./conf/bblayers.conf
	fi
	if [ "${desc}" == "sdr" ]; then
		sed -i '/meta-renesas\//a \ \ \${TOPDIR}\/..\/meta-rcar-gen3-drif \\' ./conf/bblayers.conf
		sed -i '/meta-rcar-gen3-drif/a \ \ \${TOPDIR}\/..\/meta-renesas-rea \\' ./conf/bblayers.conf
	fi
}

function setup() {
	export YOCTO='yocto-1.6.1'
	export MOPEN='dca466c074c9a35bc0133e7e0d65cca0731e2acf'
	export MLIN='8a0601723c06fdb75e62aa0f0cf15fc9d7d90167'


	# Clone the Yocto Poky repository and checkout supported revision
	if [ ! -d "poky" ]; then
		printf "${BWHITE}Yocto poky not found ...${NORMAL}\n"
		git clone  --depth 1 --single-branch --branch ${YOCTO} git://git.yoctoproject.org/poky
	fi
	printf "\n"

	if [ -d "poky" ]; then
		printf "${BWHITE}Ensuring poky is at tag %s ...${NORMAL}\n" "${YOCTO}"
		git -C ./poky checkout ${YOCTO}
	else
		printf "${BRED}poky repository not found!${NORMAL}\n"
		false
	fi
	printf "\n"


	# Get specific version of meta-openembedded
	if [ ! -d "meta-openembedded" ]; then
		printf "${BWHITE}meta-openembedded not found ...${NORMAL}\n"
		git clone git://git.openembedded.org/meta-openembedded
	fi

	if [ -d "meta-openembedded" ]; then
		printf "${BWHITE}Ensuring meta-openembedded is at %s ...${NORMAL}\n" "${MOPEN}"
		git -C meta-openembedded checkout ${MOPEN}
	else
		printf "${BRED}meta-openembedded repository not found!${NORMAL}\n"
		false
	fi
	printf "\n"


	# Get specific version of Linaro
	if [ ! -d "meta-linaro" ]; then
		printf "${BWHITE}meta-linaro not found ...${NORMAL}\n"
		git clone git://git.linaro.org/openembedded/meta-linaro.git
	fi

	if [ -d "meta-linaro" ]; then
		printf "${BWHITE}Ensuring meta-linaro is at %s ...${NORMAL}\n" "${MLIN}"
		git -C meta-linaro checkout ${MLIN}
	else
		printf "${BRED}meta-linaro repository not found!${NORMAL}\n"
		false
	fi
	printf "\n"

	# Unpack Renesas tarballs
	export RECIPES=renesas_yocto_recipes_${version}.txz
	if [ ! -d recipes ]; then
		if [ -e ${RECIPES} ]; then
			printf "${BWHITE}Extracting R-Car Yocto recipes ...${NORMAL}\n"
			tar xvf ${RECIPES}
			chmod +x ./recipes/copy*
		else
			printf "${BRED}%s not found!${NORMAL}\n" "${RECIPES}"
		fi
		printf "\n"
	fi

	export DOCS=renesas_rcar_doc_${version}.txz
	if [ ! -d Doc ]; then
		if [ -e ${DOCS} ]; then
			printf "${BWHITE}Extracting R-Car documentation ...${NORMAL}\n"
			tar xvf ${DOCS}
		else
			printf "${BRED}%s not found!${NORMAL}\n" "${DOCS}"
		fi
		printf "\n"
	fi

	export GFX=renesas_driver_gfx_${version}.txz
	if [[ "${drv}" == "" || "${drv}" == "gfx-only" || "${drv}" == "full" ]]; then
		if [ -e ${GFX} ]; then
			printf "${BWHITE}Extracting %s ...${NORMAL}\n" "${GFX}"
			tar xf ${GFX}
		else
			printf "${BRED}%s not found!${NORMAL}\n" "${GFX}"
			false
		fi
	fi

	export MMP=renesas_driver_mmp_${version}.txz
	if [[ "${drv}" == "" || "${drv}" == "full" ]]; then
		if [ -e ${MMP} ]; then
			printf "${BWHITE}Extracting %s ...${NORMAL}\n" "${MMP}"
			tar xf ${MMP}
		else
			printf "${BRED}%s not found!${NORMAL}\n" "${MMP}"
			false
		fi
	fi

	export EVAL=renesas_driver_eval_${version}.txz
	if [[ "${drv}" == "eval" ]]; then
		if [ -e ${EVAL} ]; then
			printf "${BWHITE}Extracting %s ...${NORMAL}\n" "${EVAL}"
			tar xf ${EVAL}
		else
			printf "${BRED}%s not found!${NORMAL}\n" "${EVAL}"
			false
		fi
	fi

	printf "\n"

	export MREN=recipes/meta-renesas-$version.tar.gz
	if [ ! -d "meta-renesas" ]; then
		if [ -e ${MREN} ]; then
			printf "${BWHITE}Extracting %s ...${NORMAL}\n" "${MREN}"
			tar xf ${MREN}
		else
			printf "${BRED}%s not found!${NORMAL}\n" "${MREN}"
		fi
	fi

	# Copy 
	printf "${BWHITE}Copying proprietary drivers into renesas recipes ...${NORMAL}\n"
	if [ "${drv}" == "eval" ]; then
	   ./recipes/copy_proprietary_softwares-v1100.sh ./drivers_eval/
	else
	   ./recipes/copy_proprietary_softwares-v1100.sh ./drivers/
	fi
	printf "\n"
	printf "\n"

	printf "${BWHITE}Preparing environment to build with bitbake ...${NORMAL}\n"
	# This will change our directory to ${BUILD_DIR}
	source ./poky/oe-init-build-env ${BUILD_DIR}
	# Ensure bitback executes
	bitbake --help 1>/dev/null 2>/dev/null
	printf "\n"

	#
	printf "${BWHITE}Configuring poky to build for Renesas %s devkit.${NORAML}\n" "${board}"

	if [[ "${drv}" == "" || "${drv}" == "full" ]]; then
		printf "Graphics and multimedia codec accelerator support running %s.\n" "${win}"
	fi

	if [[ "${drv}" == "bsp" ]]; then
		printf "BSP-only (no graphics and no multimedia codec accelerators, command-line only)\n"
	fi

	if [[ "${drv}" == "gfx-only" ]]; then
		printf "GFX-only (no multimedia codec accelerators) running %s.\n" "${win}"
	fi

	if [[ "${drv}" == "eval" ]]; then
		printf "Evaluation drivers - ${BRED}Graphics driver and multimedia codec accelerators cease operation after 1 hour.${NORMAL}\n"
	fi

	if [[ "${drv}" == "" || "${drv}" == "eval" || "${drv}" == "full" ]]; then
		export MBRD=../recipes/meta-$board-configs-$version.tar.gz
	else
		export MBRD=../recipes/meta-$board-configs-${drv}-$version.tar.gz
	fi
	if [ -d "conf" ]; then
		if [ -e ${MBRD} ]; then
			printf "${BWHITE}Extracting %s ...${NORMAL}\n" "${MBRD}"
			tar xf ${MBRD}
		else
			printf "${BRED}%s not found from %s!${NORMAL}\n" "${MBRD}" "${PWD}"
		fi
	else
		printf "${BRED}Missing build/conf directory!${NORMAL}\n"
	fi

	if [[ "${drv}" == "" || "${drv}" == "eval" || "${drv}" == "gfx-only" || "${drv}" == "full" ]]; then
		# Build windowing system
		cp ${board}-config/bblayers.conf ./conf
		cp ${board}-config/local-${win}.conf ./conf/local.conf
	else #if [[ "${drv}" == "bsp" ]]; then
		# No window system
		cp ${board}-config/bblayers.conf ./conf
		cp ${board}-config/local.conf ./conf/local.conf
	fi

	printf "\n"

	# Build SDK for this machine
	# Uncomment SDKMACHINE if it's commented out
	printf "${BWHITE}Configuring SDK target ${mach} ...${NORMAL}\n"
	export mach=`uname -m`
	perl -pi -e 's/^#SDKMACHINE/SDKMACHINE/' ./conf/local.conf
	perl -pi -e 's/^SDKMACHINE \?= "(.*)"/SDKMACHINE \?= "'${mach}'"/' ./conf/local.conf
	printf "SDKMACHINE set to %s\n" "${mach}"
	printf "\n"

	if [ -d "${YOCTO_DL}" ]; then
		printf "${BWHITE}Configuring download directory ${YOCTO_DL} ...${NORMAL}\n"
		perl -pi -e 's/^#DL_DIR/DL_DIR/' ./conf/local.conf
		perl -pi -e 's|^DL_DIR \?= "(.*)"|DL_DIR \?= "'${YOCTO_DL}'"|' ./conf/local.conf
	else
		printf "${YOCTO_DL} not found in $PWD\n"
	fi

	if [ -d "${YOCTO_SS}" ]; then
		printf "${BWHITE}Configuring sstate cache directory ${YOCTO_SS} ...${NORMAL}\n"
		perl -pi -e 's/^#SSTATE_DIR/SSTATE_DIR/' ./conf/local.conf
		perl -pi -e 's|^SSTATE_DIR \?= "(.*)"|SSTATE_DIR \?= "'${YOCTO_SS}'"|' ./conf/local.conf
	fi

	printf "${BWHITE}Ready to ./build.sh${NORMAL}.\n\n"
}

function bake() {
	printf "${BWHITE}Preparing environment to build with bitbake ...${NORMAL}\n"
	source ./poky/oe-init-build-env ${BUILD_DIR}
	# Ensure bitback executes
	bitbake --help 1>/dev/null 2>/dev/null
	# This will change our directory to ./build
	printf "\n"

	# The bitbake target uses weston not wayland so we swap it here
	image=${win/wayland/weston}

	# Build
	bitbake $@
}

function clean() {
	printf "${BWHITE}Preparing environment to clean with bitbake ...${NORMAL}\n"
	source ./poky/oe-init-build-env ${BUILD_DIR}
	# Ensure bitback executes
	bitbake --help 1>/dev/null 2>/dev/null
	# This will change our directory to ./build
	printf "\n"

	# The bitbake target uses weston not wayland so we swap it here
	image=${win/wayland/weston}

	# Build
	bitbake -c clean $@
}

function deploy() {
	TARGET="$1"
	shift || true
	MNT=/mnt/sd

	if [[ ${vrcar:0:1} == "1" ]]; then
		local ext="ext3"
		local image="uImage"
	elif [[ ${vrcar:0:1} == "2" ]] ; then
		local ext="ext2"
		local image="Image"
	else
		local ext="ext2"
		local image="uImage"
	fi


	if [ -z "${TARGET}" ];then
	   printf "${BYELLOW}Usage${NORMAL}: ./rea deploy <target>\n./rea deploy /dev/sdg1 (formats block device)\n./rea deploy /export/rootfs\n"
	   exit -1
	fi

	if [ ! -e "${TARGET}" ]; then
	   printf "${BYELLOW}%s${BRED} not found${NORMAL}\n" "${TARGET}"
	   exit -1
	fi

	printf "Deploying to %s\n" "${TARGET}"

	if [[ ${TARGET} == /dev/* ]]; then
	   # Format block device to prepare for new root-fs
	   printf "This will ${BYELLOW}format %s${NORMAL} !!!\n" "${TARGET}"
	   printf "${BRED}<ctrl>-C to abort${NORMAL} or ${BGREEN}<enter> to continue${NORMAL}\n"
	   read

	   printf "Formatting %s ... \n" ${ext}
	   sudo umount ${TARGET} 2>/dev/null || true
	   sudo mkfs.${ext} ${TARGET}
	   sudo mkdir -p /mnt/sd
	   sudo mount ${TARGET} /mnt/sd
	else
	   # Not a block device, assume it's an NFS share
	   MNT=${TARGET}/${BUILDNAME}
	   echo ${MNT}
	   sudo mkdir -p ${MNT}
	fi

	printf "Untarring root-filesystem ...\n"
	sudo tar xf ./${BUILD_DIR}/tmp/deploy/images/${board}/core-image-${wind}-${board}.tar.bz2 -C ${MNT}
	sudo cp ./${BUILD_DIR}/tmp/deploy/images/${board}/${image}-*-${board}.dtb ${MNT}/boot
	printf "Flushing files from cache ...\n"
	sync
	sudo umount ${TARGET} 2>/dev/null || true
	printf "${BGREEN}Done!${NORMAL}\n"
}

function tftp() {
	tftp=$1
	shift || true
	if [ "${tftp}" == "" ]; then
		tftp="/srv/tftp/linux"
	fi
	sudo mkdir -p ${tftp}/${BUILDNAME}/
	sudo cp -ax ${BUILD_DIR}/tmp/deploy/images/${board}/*Image* ${tftp}/${BUILDNAME}/
	#echo 'cp ${deploy}/${BUILDNAME}/boot/* ${tftp}/${BUILDNAME}'
	#echo "cp ${deploy}/${BUILDNAME}/boot/* ${tftp}/${BUILDNAME}"
	sudo rm -f ${tftp}/${board}
	sudo ln -srf ${tftp}/${BUILDNAME} ${tftp}/${board}
	sudo chown -h nobody:nobody ${tftp}/${BUILDNAME}/*
}

function package_sdk() {
	printf "${BWHITE}\tSDK    ...${NORMAL} "
	ln -f ./${BUILD_DIR}/tmp/deploy/sdk/*.sh ${BUILDNAME}-sdk.sh
	printf "${BGREEN}done${NORMAL} %s\n" "`pretty_size ${BUILDNAME}-sdk.sh`"
}

function package_uboot() {
	printf "${BWHITE}\tu-boot ...${NORMAL} "
	export XZ_OPT="-T0"
	mkdir -p ./${BUILD_DIR}/tmp/deploy/images/${BUILDNAME}
	cd ./${BUILD_DIR}/tmp/deploy/images
	cp -alf ./${board}/u-boot* ./${BUILDNAME}
	cp -alf ./${board}/*.srec ./${BUILDNAME}
	tar cJf ${BUILDNAME}-uboot.txz ./${BUILDNAME}/u-boot* ./${BUILDNAME}/*.srec
	cd - &>/dev/null
	mv ./${BUILD_DIR}/tmp/deploy/images/*.txz .
	printf "${BGREEN}done${NORMAL} %s\n" "`pretty_size ${BUILDNAME}-uboot.txz`"
}

function package_rootfs() {
	printf "${BWHITE}\trootfs ...${NORMAL} "
	export XZ_OPT="-T0 -0"
	mkdir -p ./${BUILD_DIR}/tmp/deploy/images/${BUILDNAME}
	cd ./${BUILD_DIR}/tmp/deploy/images
	cp -alf ./${board}/core* ./${BUILDNAME}
	rm -f ./${BUILDNAME}/*cpio.gz
	rm -f ./${BUILDNAME}/*.ext4
	ln -sf ./${BUILDNAME}/core-image-${wind}-${board}.tar.bz2 ./${BUILDNAME}/core-image.tbz2
	local date=`ls -1 ./${BUILDNAME}/core-* | awk -F\. 'NR==1{print $1}' | awk -F\- '{print $NF}'`
	tar cJf ${BUILDNAME}-rootfs.txz ./${BUILDNAME}/core-*${date}* ./${BUILDNAME}/core-image-${wind}-${board}.*
	cd - &>/dev/null
	mv ./${BUILD_DIR}/tmp/deploy/images/*.txz .
	printf "${BGREEN}done${NORMAL} %s\n" "`pretty_size ${BUILDNAME}-rootfs.txz`"
}

function package_kernel() {
	printf "${BWHITE}\tkernel ...${NORMAL} "
	export XZ_OPT="-T0"
	mkdir -p ./${BUILD_DIR}/tmp/deploy/images/${BUILDNAME}
	cd ./${BUILD_DIR}/tmp/deploy/images
	cp -alf ./${board}/* ./${BUILDNAME}
	tar cJf ${BUILDNAME}-kernel.txz ./${BUILDNAME}/*Image* ./${BUILDNAME}/modules*
	cd - &>/dev/null
	mv ./${BUILD_DIR}/tmp/deploy/images/*.txz .
	printf "${BGREEN}done${NORMAL} %s\n" "`pretty_size ${BUILDNAME}-kernel.txz`"
}

function package() {
	printf "${BWHITE}Packaging %s ... \n${NORMAL}" "${BUILDNAME}"

	package_uboot

	package_kernel

	package_rootfs

	package_sdk

	#rm -rf ./${BUILD_DIR}/tmp/deploy/images/${BUILDNAME}
}

function usbserial {
	local dev=$1
	stty -F ${dev} sane
	stty -F ${dev} 115200 -echo cs8 -cstopb -parenb -crtscts -ixon -echonl -icrnl -cooked
}

function sendfile {
	local dev=$1
	local file=$2
	local addr1=$3
	local addr2=$4

	printf "xls2\r\n" > ${dev}
	sleep 1
	printf "3\r\n" > ${dev}
	sleep 1
	printf "Y" > ${dev}
	sleep 1
	printf "Y" > ${dev}
	sleep 1
	printf "%s\r\n" "${addr1}" > ${dev}
	sleep 1
	printf "%s\r\n" "${addr2}" > ${dev}
	sleep 3

	echo "Sending [${addr1}:${addr2}] ${file} ..."
	stty -F ${dev} 115200 -echo cs8 -cstopb -parenb -crtscts -ixon -echonl -icrnl cooked
	if [ -f ${file} ]; then
        cat ${file} > ${dev}
	elif [ -f ${BUILD_DIR}/tmp/deploy/images/${board}/${file} ]; then
        cat ${BUILD_DIR}/tmp/deploy/images/${board}/${file} > ${dev}
	else
        printf "File not found \"%s\"" "${file}"
	fi
	stty -F ${dev} 115200 -echo cs8 -cstopb -parenb -crtscts -ixon -echonl -icrnl -cooked
	sleep .5
	printf "Y\r\n" > ${dev}
	sleep 1
}

function flashboot {
	local dev=$1

	usbserial ${dev}

	printf "\n"
	printf "Set DIP switches to\n"
	printf "SW1/on, SW2/on, SW3/off, SW10/OOXXOXOO (QSPI mode)\n"
	printf "Then reset into the mini-monitor\n"
	printf "Press <enter> to continue\n"
	read

	printf "Now set DIP switches back:\n"
	printf "SW1/off, SW2/off, SW3/on\n"
	printf "\nPress <enter> when you have set the DIP switches\n"
	printf "\n"
	printf "You could/should monitor this process while flashing\n"
	printf "e.g. Run minicom and verify you are at the mini-monitor prompt\n"
	printf "Once you continue you should see the script sending commands\n"
	printf "\n"
	read

	printf "Entering flash mode ...\n"
	sendfile ${dev} bootparam_sa0.srec   E6320000 000000
	sendfile ${dev} bl2-${board}.srec    E6302000 040000
	sendfile ${dev} cert_header_sa6.srec E6320000 180000
	sendfile ${dev} bl31-${board}.srec   44000000 1C0000
	sendfile ${dev} tee-${board}.srec    44100000 200000
	if [ "$vrcar" == "2.16.0" ]; then
	sendfile ${dev} u-boot-elf.srec      50000000 640000
	else
	sendfile ${dev} u-boot-elf.srec      49000000 640000
	fi

	printf "\nPower Off, set SW10 to Hyperflash mode, and power on to start U-Boot\n"
	printf "\nH3 1.0 Hyperflash mode: SW10/OOXXOOXO\n"
	printf "\nH3 1.1 Hyperflash mode: SW10/OOXOOOXX\n"

	printf "\n***IMPORTANT*** Verify SW5, SW6, SW7 (on bottom of board) are set to 1 not open/neutral.\n"
}

action=$1
shift || true
case "${action}" in
	config)
		# Already run above
		#config
		;;

	setup)
		if [[ ${vrcar:0:1} == "1" ]] ; then
			setup
		elif [[ ${vrcar:0:1} == "2" ]] ; then
			if [[ ${vrcar:2:1} == "7" ]] ; then
				setup2 $poky_v270 $linaro_v270 $openembedded_v270 $renesas_v270
			elif [[ ${vrcar:2:1} == "8" ]] ; then
				setup2 $poky_v280 $linaro_v280 $openembedded_v280 $renesas_v280
			elif [[ ${vrcar:2:1} == "9" ]] ; then
				setup2 $poky_v290 $linaro_v290 $openembedded_v290 $renesas_v290
			elif [[ ${vrcar:2:2} == "12" ]] ; then
				setup2 $poky_v2120 $linaro_v2120 $openembedded_v2120 $renesas_v2120
			elif [[ ${vrcar:2:2} == "16" ]] ; then
				setup2 $poky_v2160 $linaro_v2160 $openembedded_v2160 $renesas_v2160
			elif [[ ${vrcar:2:2} == "19" ]] ; then
				setup2 $poky_v2190 $linaro_v2190 $openembedded_v2190 $renesas_v2190
			elif [[ ${vrcar:2:2} == "23" ]] ; then
				setup2 $poky_v2230 $linaro_v2230 $openembedded_v2230 $renesas_v2230
			else
				printf "Unknown version %s\n" ${vrcar}
			fi
		else
			printf "no ver\n"
		fi
		;;

	bake)
		bake "$@"
		;;

	clean)
		clean "$@"
		;;

	# -k continues after a failure up to dependencies
	core)
		bake $@ -k core-image-${wind}
		;;

	sdk)
		bake $@ -k core-image-${wind}-sdk -c populate_sdk
		;;

	image)
		curdir=`pwd`
		bake $@ -k core-image-${wind}
		cd ${curdir}
		bake $@ -k core-image-${wind}-sdk -c populate_sdk
		;;

	deploy)
		deploy "$@"
		;;
	tftp)
		tftp "$@"
		;;

	package)
		package
		;;
	package_sdk)
		package_sdk
		;;
	package_uboot)
		package_uboot
		;;
	package_kernel)
		package_kernel
		;;
	package_rootfs)
		package_rootfs
		;;

	fix_local_conf)
		cd ${BUILD_DIR}
		fix_local_conf
		cd -
		;;

	fix_bblayer_conf)
		cd ${BUILD_DIR}
		fix_bblayer_conf
		cd -
		;;

	push)
		printf "TODO: This will push the given package to a target.\n./rea push <package> <target>"
		scp ${BUILD_DIR}tmp/deploy/ipk/$1 $2
		;;

	usbserial)
		usbserial "$@"
		;;

	flashboot)
		tty=$1
		shift || true
		if [ "${tty}" == "" ]; then
			tty=${DEFAULT_TTY}
			printf "Defaulting to %s\n" ${tty}
		fi
		flashboot ${tty}
		;;
		
    sendfile)
        echo sendfile $@
        sendfile $@
        ;;

	uboot)
		utype=$1
		shift || true
		tty=$1
		shift || true
		if [ "${tty}" == "" ]; then
			tty=${DEFAULT_TTY}
			printf "Defaulting to %s\n" ${tty}
		fi
		usbserial ${tty}
		case "${utype}" in
			sd)
				printf "./rea uboot sd not implemented yet\n" && false
				printf "set bootargs_sd 'mem=2048M consoleblank=0 console=ttySC0,115200 rw root=/dev/mmcblk1p1 rootwait relative_sleep_states=1 cma=384MB ip=192.168.0.30'\r\n" >> ${tty}
				sleep .1
				printf "set bootcmd_sd 'mmc rescan;ext4load mmc 0:1 0x48080000 /boot/Image;ext4load mmc 0:1 0x48000000 /boot/Image-r8a7795-salvator-x.dtb;booti 0x48080000 - 0x48000000'" >> ${tty}
				sleep .1
				printf "set bootcmd 'set bootargs $bootargs_sd;run $bootcmd_sd'\r\n" >> ${tty}
				sleep .1
				printf "save\r\n" >> ${tty}
				sleep .5
				printf "reset\r\n" >> ${tty}
				;;
			nfs)
				printf "set bootargs_nfs 'console=ttySC0,115200 mem=2048M consoleblank=0 rw root=/dev/nfs nfsroot=192.168.0.1:/srv/export/rfs/linux/${board} ip=192.168.0.20'\r\n" > ${tty}
				sleep .1
				printf "set bootcmd_nfs 'tftp 0x48080000 linux/${board}/boot/Image;tftp 0x48000000 linux/${board}/boot/${board}.dtb;booti 0x48080000 - 0x48000000'\r\n" > ${tty}
				sleep .1
				printf "set bootcmd 'set bootargs $bootargs_nfs;run $bootcmd_nfs'\r\n" >> ${tty}
				sleep .1
				printf "save\r\n" >> ${tty}
				sleep .5
				printf "reset\r\n" >> ${tty}
				;;
			mac)
				if [[ "$2" == "" ]]; then
					echo './rea uboot mac <MAC addr>'
					echo 'e.g. ./rea uboot mac 2E:09:0A:xx:xx:xx'
					echo '(There should be a sticker on the RJ-45 connector with the MAC address to use.)'
				else
					echo Setting MAC to $2
					printf "set ethaddr %s\r\n" "$2" >> ${tty}
					sleep .1
				fi
				;;
			save)
				printf "save\r\n" >> ${tty}
				sleep .5
				printf "reset\r\n" >> ${tty}
				;;
			dtb)
				printf "${BRED}Not Yet Implemented: Eventually this command will load the dtb and save it to SIP flash${NORMAL}.\n" && false
				#Upload dtb and store in board flash
				if ! which srec_cat &>/dev/null; then
					sudo apt-get -y install srecord
				fi
				usbserial ${tty}
				printf "loads 0x48000000\r\n" > ${tty}
				srec_cat ${BUILD_DIR}/tmp/deploy/images/${board}/Image-*-${board}.dtb -binary -address_length=4 -DIS Data_Count -Execution_Start_Address 0x48000000 > ${tty}
				;;
			kernel)
				usbserial ${tty}
				printf "loads 0x48080000\r\n" > ${tty}
				srec_cat ${BUILD_DIR}/tmp/deploy/images/${board}/Image -binary -address_length=4 -DIS Data_Count -Execution_Start_Address 0x48080000 > ${TTY}
				;;
			*)
				printf "Usage: ./rea uboot {${BBLUE}sd${NORMAL}|${BBLUE}nfs${NORMAL}|${BBLUE}mac${NORMAL}}\n"
				;;
		esac
		;;

	*)
		printf "Usage: ./rea {${BYELLOW}config${NORMAL}|${BGREEN}setup${NORMAL}|${BBLUE}bake <pkg>${NORMAL}|${BBLUE}clean <pkg>${NORMAL}|${BBLUE}core${NORMAL}|${BBLUE}sdk${NORMAL}|${BBLUE}image${NORMAL}|${BBLUE}deploy <path>${NORMAL}|${BBLUE}package${NORMAL}|${BMAGENTA}usbserial${NORMAL}|${BMAGENTA}flashboot${NORMAL}}\n"

		printf "\n${BYELLOW}config${NORMAL}\n"
		printf "\tPrints the current configuration and write a default configuraiton if one does not exist.\n"

		printf "\n${BGREEN}setup${NORMAL}\n"
		printf "\tPerforms critical configuration setup.\n"
		printf "\tSetup must be run prior to executing one of the build commands.\n"
		printf "\tCreates build directory\n\tModifies the bblayer.conf\n\tClones meta repositories and checkouts to requisite commits/tags\n\tPlaces proprietary Renesas driver tarballs into kernel build\n"
		printf "\t(After setup is run <config>/conf/bblayers.conf should be edited to add custom bitbake layers.)\n"

		printf "\n${BBLUE}core${NORMAL}\n"
		printf "\tBuilds the root file-system for the configured Yocto/bitbake overlays\n"

		printf "\n${BBLUE}sdk${NORMAL}\n"
		printf "\tBuilds an x86_64/ARM cross-compiling SDK for the configured Yocto/bitbake overlays\n"
		printf "\t(Modify the SDKMACHINE variable in the <config>/conf/local.conf file to change from x86_64.)\n"

		printf "\n${BBLUE}bake ${BRED}<package>${NORMAL}\n"
		printf "\tBuild specified package with bitbake\n"
		printf "\te.g. ./rea bake dtc\n"

		printf "\n${BBLUE}flashboot${NORMAL} [${BRED}<tty>${NORMAL}]\n"
		printf "\tAutomates the download of the multiple boot-flash files required for generation 3 devices\n"
		printf "\t./rea flashboot /dev/ttyUSB0\n"
		printf "\n"
		;;
esac
